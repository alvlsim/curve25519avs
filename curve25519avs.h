/*
*	Name:        curve25519avs.h
*	Purpose:     RFC7748 implementation of Curve25519 functions
*	Author:      Aleksandr Simonenko
*	Copyright:   (c) 2017 Aleksandr Simonenko <arrechucho@yandex.ru>
*	Date:        01/06/2017
*	Licence:     <http://unlicense.org/>
*/

#ifndef CURVE25519AVS_H
#define CURVE25519AVS_H
#include <stdint.h>

#ifdef  __cplusplus
extern "C" {
#endif

// k    - Input scalar
// u    - Input u-coordinate
// uOut - Output u-coordinate
void X25519(uint8_t *uOut,uint8_t *k,uint8_t *u);

#ifdef  __cplusplus
}
#endif

#endif // CURVE25519AVS_H
