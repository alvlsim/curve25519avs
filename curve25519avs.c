/*
*	Name:        curve25519avs.c
*	Purpose:     RFC7748 implementation of Curve25519 functions
*	Author:      Aleksandr Simonenko
*	Copyright:   (c) 2017 Aleksandr Simonenko <arrechucho@yandex.ru>
*	Date:        01/06/2017
*	Licence:     <http://unlicense.org/>
*/


#include "curve25519avs.h"


#define false 0
#define true 1
typedef int bool;


uint8_t P25519[32]; // Modulus P = 2**255-19


// D - 64 bytes
// Fill Modulus P25519 before using!!!
void Mod25519(uint8_t* D);

// Sum of two 32 bytes values modulo P:  (a + b) % P
// res - output /32 bytes/
void Sum32mod25519(uint8_t *res,uint8_t *a,uint8_t *b);

// Difference between two 32 bytes values modulo P:  (a - b) % P
// res - output /32 bytes/
void Sub32mod25519(uint8_t *res,uint8_t *a,uint8_t *b);

// Square of 'a' modulo P
// res - output /64 bytes/
void Sqr32mod25519(uint8_t *res,uint8_t *a);

// res - output /64 bytes/
void Mul32mod25519(uint8_t *res, uint8_t *a, uint8_t *b);

// res - output /64 bytes/
// a - input /64 bytes/
// tres, s1, s2 - auxiliary arrays to store intermediary calculations /64 bytes/
void Pow32mod25519(uint8_t *res, uint8_t *a, uint8_t *tres, uint8_t *s1, uint8_t *s2);

// Conditional Swap
void cswap32(bool cswap, uint8_t *a, uint8_t *b, uint8_t *dummy);



//=================================================================================================
//============================================ X25519 =============================================
//=================================================================================================
void X25519(uint8_t *uOut,uint8_t *k,uint8_t *u)
{
    int i;
    uint8_t x_1[64],x_2[64],x_3[64],z_2[64],z_3[64];
    uint8_t A[32],B[32],C[32],D[32],E[32];
    uint8_t AA[64],BB[64],DA[64],CB[64];

    uint8_t a24[64]; // a24=121665 0x1DB41

    bool k_t;
    bool cswap;
    int  bytep=31;
    int  bitp=7;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    // Fill Modulus P = 2**255-19
    for(i=1;i<31;i++) P25519[i]=0xFF; P25519[i]=0x7F; P25519[0]=0xED; // 11101101  250 '1's in a row

    // Fill a24
    a24[0]=0x41; a24[1]=0xDB; a24[2]=0x01; for(i=3;i<64;i++) { a24[i]=0; }

    k[0]  &=0xF8; // Clear 3 least significant bits
    k[31] &=0x7F; // Clear most significant bit
    k[31] |=0x40; // Set the second most significant bit

    // implementations of X25519 MUST mask the most significant bit
    // in the final byte of Input u-coordinate
    u[31] &=0x7F; // Clear most significant bit

    // x_1 = u
    // x_3 = u
    for(i=0;i<32;i++) { x_1[i]=u[i]; x_3[i]=u[i]; }
    while(i<64) { x_1[i]=0; x_3[i]=0; i++; }

    // x_2 = 1
    // z_3 = 1
    for(i=1;i<64;i++) { x_2[i]=0; z_3[i]=0; } x_2[0]=1; z_3[0]=1;

    // z_2 = 0
    for(i=0;i<64;i++) { z_2[i]=0; }

    // swap = 0
    cswap=false;

    // For t = bits-1 down to 0:
    // pass 'k's bits: start from k[31].6
    for(int t=254;t>=0;t--)
    {
        // k_t = (k >> t) & 1
        bitp--; if(bitp<0) { bitp=7; bytep--; }
        k_t=(k[bytep]>>bitp)&0x01;

        cswap^=k_t;                 // swap ^= k_t

        // Conditional swap
        cswap32(cswap,x_2,x_3,AA);  // (x_2, x_3) = cswap(swap, x_2, x_3)     // AA as dummy
        cswap32(cswap,z_2,z_3,AA);  // (z_2, z_3) = cswap(swap, z_2, z_3)     // AA as dummy

        cswap=k_t;                  // swap = k_t

        Sum32mod25519(A,x_2,z_2);   // A = x_2 + z_2
        Sqr32mod25519(AA,A);        // AA = A^2
        Sub32mod25519(B,x_2,z_2);   // B = x_2 - z_2
        Sqr32mod25519(BB,B);        // BB = B^2
        Sub32mod25519(E,AA,BB);     // E = AA - BB
        Sum32mod25519(C,x_3,z_3);   // C = x_3 + z_3
        Sub32mod25519(D,x_3,z_3);   // D = x_3 - z_3
        Mul32mod25519(DA,D,A);      // DA = D * A
        Mul32mod25519(CB,C,B);      // CB = C * B
        Sum32mod25519(A,DA,CB);                                  // 'A' as temp, not used anymore
        Sqr32mod25519(x_3,A);       // x_3 = (DA + CB)^2
        Sub32mod25519(A,DA,CB);                                  // 'A' as temp, not used anymore
        Sqr32mod25519(x_2,A);                                    // 'x_2' as temp, calculated later
        Mul32mod25519(z_3,x_1,x_2); // z_3 = x_1 * (DA - CB)^2
        Mul32mod25519(x_2,AA,BB);   // x_2 = AA * BB
        Mul32mod25519(BB,a24,E);                                 // 'BB' as temp, not used anymore
        Sum32mod25519(A,AA,BB);
        Mul32mod25519(z_2,E,A);     // z_2 = E * (AA + a24 * E)
    }

    // Conditional swap
    cswap32(cswap,x_2,x_3,AA);      // (x_2, x_3) = cswap(swap, x_2, x_3)   // AA as dummy
    cswap32(cswap,z_2,z_3,AA);      // (z_2, z_3) = cswap(swap, z_2, z_3)   // AA as dummy

    Pow32mod25519(AA,z_2,BB,DA,CB);
    Mul32mod25519(uOut,x_2,AA);     // Return x_2 * (z_2^(p - 2))
}


//=================================================================================================
//=========================================== Mod25519 ============================================
//=================================================================================================
void Mod25519(uint8_t* D)
{
    int i,k,t,m,onePos,passPos;
    int bytepMod,bitpMod,bytep,bitp;
    int byteStart,bitStart;
    int J=0xFFED00;
    bool borrow;
    bool modPosShifted;
    uint8_t modCur[64];
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    while(true)
    {
        // Locate most significant bit '1'
        onePos=-1; bytep=64; bitp=0;

        while(true)
        {
            onePos++;
            bitp--; if(bitp<0) { bitp=7; bytep--; if(bytep<0) break; }
            if((D[bytep]>>bitp)&1) break;
        }
        if(onePos>257) return; // operand less than modulus

        // fix position of the most significant bit of operand
        byteStart=bytep; bitStart=bitp;

        passPos=onePos; bytepMod=31; bitpMod=6; modPosShifted=false;

        // Find first greater bit
        while(true)
        {
            passPos++;
            bitp--;    if(bitp<0)    { bitp=7;    bytep--;    if(bytep<0)    break; }
            bitpMod--; if(bitpMod<0) { bitpMod=7; bytepMod--; if(bytepMod<0) break; }

            t=(D[bytep]>>bitp)&1;
            m=(P25519[bytepMod]>>bitpMod)&1;

            if(t>m) // start modulus at onePos
            {
                break;
            }
            else if(t<m) // start modulus at onePos+1
            {
                if(onePos==257) return; // no place to shift modulus
                modPosShifted=true;
                break;
            }
        }

        // operand starts with the same sequence of bits as that of modulus's
        if((bytepMod<0)&&(bytep<0)) // operand equals modulus, return 0 result
        {
            for(i=0;i<64;i++) D[i]=0; return;
        }
        else
        {
            m=byteStart; // save operand's start position
            // Shift also if bytepMod<0 AND bytep>=0
            if(modPosShifted) { bitStart--; if(bitStart<0) { bitStart=7; byteStart--; } }

            for(k=63;k>byteStart;k--) modCur[k]=0; // start clear modCur
            if(bitStart==6) // copy modulus as is
            {
                for(i=31;i>=0;i--,k--) modCur[k]=P25519[i];
            }
            else if(bitStart==7)
            {
                for(i=31;i>0;i--,k--) modCur[k]=0xFF; modCur[k]=0xDA; k--;
            }
            else
            {
                modCur[k]=0x7F>>(6-bitStart); k--;
                for(i=30;i>0;i--,k--) modCur[k]=0xFF;
                modCur[k]=(J>>(14-bitStart))&0xFF; k--;
                modCur[k]=(J>>(6-bitStart))&0xFF; k--;
            }
            while(k>=0) { modCur[k]=0; k--; } // end clear modCur
        }

        // Subtract shifted modulus from the operand
        // here operand D>modulus always
        borrow=false;
        for(i=0;i<=m;i++) //byteStart
        {
            if(borrow) { if(D[i]) { D[i]--; borrow=false; } else D[i]=0xFF; }

            if(D[i]>=modCur[i]) D[i]-=modCur[i];
            else { t=D[i]+0x100; t-=modCur[i]; D[i]=t; borrow=true; }
        }
    }
}


//=================================================================================================
//========================================= Sum32mod25519 =========================================
//=================================================================================================
// NOTE: 'a' and 'b' are in the range 0...P-1

void Sum32mod25519(uint8_t *res,uint8_t *a,uint8_t *b)
{
    bool carry=false;
    bool borrow=false;
    bool resMax=false;
    int  i,sumByte,difByte;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    for(i=0;i<32;i++)
    {
        sumByte=a[i]+b[i]; if(carry) { sumByte++; carry=false; }
        if(sumByte>0xFF) { res[i]=sumByte&0xFF; carry=true; }
        else             { res[i]=sumByte; }
    }

    // check if result greater than modulus P
    for(i=31;i>=0;i--)
    {
        if(res[i]>P25519[i]) { resMax=true; break; }
        else if(res[i]<P25519[i])  {  break; }
    }

    if(resMax) // subtract P from res
    {
        for(i=0;i<32;i++)
        {
            difByte=res[i]-P25519[i];
            if(borrow) { difByte--; borrow=false; }
            if(difByte<0) { difByte+=0x100; borrow=true; }
            res[i]=difByte;
        }
    }
    else if(i<0) // res[i]==P25519[i]
    {
        for(i=0;i<32;i++) res[i]=0;
    }
}


//=================================================================================================
//========================================= Sub32mod25519 =========================================
//=================================================================================================
// NOTE: 'a' and 'b' are in the range 0...P-1

void Sub32mod25519(uint8_t *res, uint8_t *a, uint8_t *b)
{
    bool borrow=false;
    bool carry=false;
    int  i,difByte,sumByte;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    for(i=0;i<32;i++)
    {
        difByte=a[i]-b[i];
        if(borrow) { difByte--; borrow=false; }
        if(difByte<0) { difByte+=0x100; borrow=true; }
        res[i]=difByte;
    }

    if(borrow) // result is negative, add P
    {
        for(i=0;i<32;i++)
        {
            sumByte=res[i]+P25519[i]; if(carry) { sumByte++; carry=false; }
            if(sumByte>0xFF) { res[i]=sumByte&0xFF; carry=true; }
            else             { res[i]=sumByte; }
        }
    }
}


//=================================================================================================
//========================================= Sqr32mod25519 =========================================
//=================================================================================================
void Sqr32mod25519(uint8_t *res,uint8_t *a)
{
    bool carry;
    int mul,shift,sumByte;
    uint8_t uch, uchLow;
    uint8_t uchHigh=0;
    int i,j,k;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    for(i=0;i<64;i++) res[i]=0;

    for(j=0,shift=0;j<32;j++,shift++)
    {
        uch=a[j]; carry=false; uchHigh=0;
        for(i=0,k=shift;i<32;i++,k++)
        {
            mul=uch*a[i]+uchHigh; uchHigh=mul>>8; uchLow=mul&0xFF;

            sumByte=res[k]+uchLow; if(carry) { sumByte++; carry=false; }
            if(sumByte>0xFF) { res[k]=sumByte&0xFF; carry=true; }
            else             { res[k]=sumByte; }

            if(i==31) { k++; if(carry) uchHigh++; res[k]=uchHigh; }
        }
    }
    Mod25519(res);
}


//=================================================================================================
//========================================= Mul32mod25519 =========================================
//=================================================================================================
void Mul32mod25519(uint8_t *res, uint8_t *a, uint8_t *b)
{
    bool carry;
    int mul,shift,sumByte;
    uint8_t uch, uchLow;
    uint8_t uchHigh=0;
    int i,j,k;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    for(i=0;i<64;i++) res[i]=0;

    for(j=0,shift=0;j<32;j++,shift++)
    {
        uch=b[j]; carry=false; uchHigh=0;
        for(i=0,k=shift;i<32;i++,k++)
        {
            mul=uch*a[i]+uchHigh; uchHigh=mul>>8; uchLow=mul&0xFF;

            sumByte=res[k]+uchLow; if(carry) { sumByte++; carry=false; }
            if(sumByte>0xFF) { res[k]=sumByte&0xFF; carry=true; }
            else             { res[k]=sumByte; }

            if(i==31) { k++; if(carry) uchHigh++; res[k]=uchHigh; }
        }
    }
    Mod25519(res);
}


//=================================================================================================
//========================================= Pow32mod25519 =========================================
//=================================================================================================
// Return (a^(p - 2))
// p=2^255 - 19
// P-2 = 0x7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeb // 11101011

void Pow32mod25519(uint8_t *res, uint8_t *a, uint8_t *tres, uint8_t *s1, uint8_t *s2)
{

    // tail = 1

    // a1 = a0^2 mod P
    Sqr32mod25519(s1,a);
    // tail = tail*a0 mod P
    // Here tail=a0

    // a2 = a1^2 mod P
    Sqr32mod25519(s2,s1);
    // tail = tail*a mod P
    Mul32mod25519(tres,s1,a);

    // a3 = a2^2 mod P
    Sqr32mod25519(s1,s2);

    // a4 = a3^2 mod P
    Sqr32mod25519(s2,s1);
    // tail = tail*a mod P
    Mul32mod25519(res,s1,tres);

    // a5 = a4^2 mod P
    Sqr32mod25519(s1,s2);

    for(int i=0;i<125;i++)
    {
        // a = a^2 mod P
        Sqr32mod25519(s2,s1);
        // tail = tail*a mod P
        Mul32mod25519(tres,s1,res);

        // a = a^2 mod P
        Sqr32mod25519(s1,s2);
        // tail = tail*a mod P
        Mul32mod25519(res,s2,tres);
    }
}


//=================================================================================================
//============================================ cswap32 ============================================
//=================================================================================================
void cswap32(bool cswap, uint8_t *a, uint8_t *b, uint8_t *dummy)
{

//   Where mask(swap) is the all-1 or all-0 word of the same length as x_2
//   and x_3, computed, e.g., as mask(swap) = 0 - swap.

//     dummy = mask(swap) AND (x_2 XOR x_3)
//     x_2 = x_2 XOR dummy
//     x_3 = x_3 XOR dummy
//     Return (x_2, x_3)

    int i;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if(cswap) for(i=0;i<32;i++) dummy[i]=0xFF&(a[i]^b[i]);
    else      for(i=0;i<32;i++) dummy[i]=0x00&(a[i]^b[i]);

    for(i=0;i<32;i++) { a[i]=a[i]^dummy[i]; b[i]=b[i]^dummy[i]; }
}

